package com.example.tp2;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        long id = getIntent().getExtras().getInt("Num");
        System.out.println(id);

        final WineDbHelper tryit = new WineDbHelper(this);
        Cursor vin = tryit.fetchAllWines();
        boolean test = false;
        while(!test)
        {
            if(vin.getLong(0) == id)
            {
                break;
            }
            vin.moveToNext();
        }
        final Wine newvin = WineDbHelper.cursorToWine(vin);

        TextView title = (TextView) findViewById(R.id.wineName);
        final EditText region = (EditText) findViewById(R.id.editWineRegion);
        final EditText loc = (EditText) findViewById(R.id.editLoc);
        final EditText clim = (EditText) findViewById(R.id.editClimate);
        final EditText area = (EditText) findViewById(R.id.editPlantedArea);
        Button but = (Button) findViewById(R.id.button);


        title.setText(newvin.getTitle());
        region.setText(newvin.getRegion());
        loc.setText(newvin.getLocalization());
        clim.setText(newvin.getClimate());
        area.setText(newvin.getPlantedArea());

        but.setClickable(true);
        but.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newvin.setRegion(String.valueOf(region.getText()));
                newvin.setLocalization(String.valueOf(loc.getText()));
                newvin.setClimate(String.valueOf(clim.getText()));
                newvin.setPlantedArea(String.valueOf(area.getText()));

                tryit.updateWine(newvin);
                System.out.println("Updated table");

            }
        });

    }
}
