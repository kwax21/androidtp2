package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

	// db.execSQL() with the CREATE TABLE ... command

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME + " TEXT NOT NULL," + COLUMN_WINE_REGION + " TEXT NOT NULL," +COLUMN_LOC + " TEXT NOT NULL," +COLUMN_CLIMATE + " TEXT NOT NULL," +COLUMN_PLANTED_AREA + " TEXT NOT NULL);");
        this.populate(db);
        System.out.println("ici");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine, SQLiteDatabase db) {
        // Inserting Row
        long id = 0;
        ContentValues val = new ContentValues();

        val.put(COLUMN_NAME, wine.getTitle());
        val.put(COLUMN_WINE_REGION, wine.getRegion());
        val.put(COLUMN_LOC, wine.getLocalization());
        val.put(COLUMN_CLIMATE, wine.getClimate());
        val.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        id = db.insert(TABLE_NAME, null, val);
        //db.close(); // Closing database connection

        return (id != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = -1;

        ContentValues val = new ContentValues();

        val.put(COLUMN_CLIMATE, wine.getClimate());
        val.put(COLUMN_LOC, wine.getLocalization());
        val.put(COLUMN_NAME, wine.getTitle());
        val.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        val.put(COLUMN_WINE_REGION, wine.getRegion());
        db.update(TABLE_NAME, val, _ID+"=?", new String[] {Long.toString(wine.getId())});

        return res;

    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

	        // call db.query()
        final String[] COL = {_ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA};
        final String[] NONE = {};
        Cursor curseur;
        curseur = db.query(TABLE_NAME, COL, "", NONE, "", "", "", "");

        if (curseur != null) {
            curseur.moveToFirst();
        }
        return curseur;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.close();
    }

     public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"), db);
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"), db);
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"), db);
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"), db);
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"), db);
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"), db);
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"), db);
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"), db);
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"),db);
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"), db);
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"), db);


        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        //db.close();
    }


    public static Wine cursorToWine(Cursor curs) {
        Wine vin = new Wine(curs.getInt(0), curs.getString(1), curs.getString(2), curs.getString(3), curs.getString(4), curs.getString(5));
        return vin;
    }
}
