package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final ListView lists = (ListView) findViewById(R.id.liste);
        WineDbHelper dbh;
        dbh = new WineDbHelper(lists.getContext());
        dbh.getWritableDatabase();
        Cursor curseur = dbh.fetchAllWines();

        List<Map<String, String>> vins = new ArrayList<Map<String, String>>();
        while(!curseur.isAfterLast())
        {
            Map<String, String> vinss = new HashMap<String, String>(3);
            vinss.put("Num", Long.toString(curseur.getLong(curseur.getColumnIndex(WineDbHelper._ID))));
            vinss.put("Nom", curseur.getString(1));
            vinss.put("Loc", curseur.getString(2));
            vins.add(vinss);
            curseur.moveToNext();
        }

        SimpleAdapter adapter;
        adapter = new SimpleAdapter(this, vins, android.R.layout.simple_list_item_2, new String[] {"Nom", "Loc"}, new int[] {android.R.id.text1, android.R.id.text2});
        lists.setAdapter(adapter);

        lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Integer.valueOf( ((HashMap<String, String>) lists.getItemAtPosition(i)).get("Id") ));

                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                System.out.println(((HashMap<String, String>) lists.getItemAtPosition(i)).get("Num"));
                intent.putExtra("Num", Integer.parseInt(((HashMap<String, String>) lists.getItemAtPosition(i)).get("Num")));
                startActivity(intent);

            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
